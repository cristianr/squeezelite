<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup>
    <Filter Include="Header Files">
      <UniqueIdentifier>{93995380-89BD-4b04-88EB-625FBE52EBFB}</UniqueIdentifier>
      <Extensions>h;hpp;hxx;hm;inl;inc;xsd</Extensions>
    </Filter>
    <Filter Include="Resource Files">
      <UniqueIdentifier>{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}</UniqueIdentifier>
      <Extensions>rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx</Extensions>
    </Filter>
    <Filter Include="Source Files">
      <UniqueIdentifier>{4FC737F1-C7A5-4376-A066-2A32D752A2FF}</UniqueIdentifier>
      <Extensions>cpp;c;cc;cxx;def;odl;idl;hpj;bat;asm;asmx</Extensions>
    </Filter>
  </ItemGroup>
  <ItemGroup>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\all.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\assert.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\callback.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libvorbis-1.3.4\include\vorbis\codec.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libogg-1.3.1\include\ogg\config_types.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include=".include\flac-1.2.1\include\FLAC\export.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include=".include\flac-1.2.1\include\FLAC\format.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libmad-0.15.1b\mad.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\metadata.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\mpg123-1.17.0\src\libmpg123\mpg123.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\faad2-2.7\include\neaacdec.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libogg-1.3.1\include\ogg\ogg.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\ordinals.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libogg-1.3.1\include\ogg\os_types.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\portaudio.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="slimproto.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\soxr.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="squeezelite.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\stream_decoder.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\flac-1.2.1\include\FLAC\stream_encoder.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="include\libvorbis-1.3.4\include\vorbis\vorbisenc.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="Header.h">
      <Filter>Header Files</Filter>
    </ClInclude>
    <ClInclude Include="resource.h">
      <Filter>Header Files</Filter>
    </ClInclude>
  </ItemGroup>
  <ItemGroup>
    <ClCompile Include="buffer.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="decode.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="faad.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="ffmpeg.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="flac.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="mad.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="main.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="mpg.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="output.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="pcm.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="process.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="resample.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="slimproto.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="stream.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="utils.c">
      <Filter>Source Files</Filter>
    </ClCompile>
    <ClCompile Include="vorbis.c">
      <Filter>Source Files</Filter>
    </ClCompile>
  </ItemGroup>
  <ItemGroup>
    <ResourceCompile Include="squeezelite.rc">
      <Filter>Resource Files</Filter>
    </ResourceCompile>
  </ItemGroup>
</Project>